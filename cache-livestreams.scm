(define-module (cache-livestreams)
  #:use-module (web client)
  #:use-module (web response)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (xdg-base-dir)
  #:export (cache-livestreams))


(define %following (string-append (xdg-data-home) "/cache-livestreams/following"))
(define %cache (string-append (xdg-cache-home) "/livestreams"))


(define (online? user)
  (let* ((response (http-request (format #f "https://twitch.tv/~a" user)))
         (html (if (= (response-code response) 301)
                   (receive (_resp resp-body)
                       (http-request (response-location response))
                     resp-body)
                   (error "Not getting redirected (got ~a response)" (response-code response))))
         (online? (string-contains html "isLiveBroadcast")))
    (format (current-output-port) "~:[~16,,,@a~;~16,,,'─@a ⇐~]~%" online? user)
    online?))

(define (cache-livestreams)
  (let* ((following (call-with-input-file %following
                      (lambda (in)
                        (let users-from-port ((users '[])
                                              (user (get-line in)))
                          (if (eof-object? user)
                              (reverse users)
                              (users-from-port (cons user users)
                                               (get-line in)))))))
         (online-users (filter online? following)))
    (call-with-output-file %cache
      (lambda (out)
        (format out "~{~a~%~}" online-users)))
    (format (current-output-port) "Finished!~%")))
